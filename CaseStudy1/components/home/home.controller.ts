﻿/*
    File Name: home.ts - controler for home.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class HomeController {
    label: string;

    constructor() {
        this.label = "LexCorp";
    } 
}
app.controller("HomeController", [HomeController]);