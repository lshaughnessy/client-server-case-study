/*
    File Name: home.ts - controler for home.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
var HomeController = (function () {
    function HomeController() {
        this.label = "LexCorp";
    }
    return HomeController;
})();
app.controller("HomeController", [HomeController]);
//# sourceMappingURL=home.controller.js.map