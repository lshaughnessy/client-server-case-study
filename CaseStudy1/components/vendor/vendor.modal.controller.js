/*
    File Name: vendor.modal.controller.ts - controller for vendorModal.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
var VendorModalController = (function () {
    /*
        constructor
        @param modalInstance: instance of modal not the same as $modal
        @param vendor: vendor instance from in injector loaded from options
    */
    function VendorModalController(modal, vendor) {
        this.modal = modal;
        this.vendor = vendor;
        this.vendor = vendor;
        if (vendor) {
            this.modalTitle = "Update Details for Vendor" + vendor.vendorno;
            this.todo = "update";
        } //end if
        else {
            this.modalTitle = "Add Details for New Vendor";
            this.todo = "add";
        } //end else
        this.retVal = { operation: "", retVendor: vendor, status: "" };
    } //constructor
    /*
        NAME:    add()
        PURPOSE: send new vendor back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    VendorModalController.prototype.add = function () {
        this.retVal.operation = "add";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }; //add
    /*
       NAME:    cancel()
       PURPOSE: discard any changes then back to the main controller
       ACCEPTS: N/A
       RETURNS: N/A
   */
    VendorModalController.prototype.cancel = function () {
        this.retVal.operation = "cancel";
        if (this.vendor) {
            this.retVal.status = "Vendor not changed! ";
        } //end if
        else {
            this.retVal.status = "No Vendor Entered";
        } //end else
        this.modal.close(this.retVal);
    }; //cancel
    /*
       NAME:    update()
       PURPOSE: send modified vendor back to the main controller
       ACCEPTS: N/A
       RETURNS: N/A
   */
    VendorModalController.prototype.update = function () {
        this.retVal.operation = "update";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }; //update
    /*
       NAME:    delete()
       PURPOSE: send vendor back to the main controller
       ACCEPTS: N/A
       RETURNS: N/A
   */
    VendorModalController.prototype.delete = function () {
        this.retVal.operation = "delete";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }; //delete
    //static injection
    VendorModalController.$inject = ["$modalInstance", "modalData"]; //modalData from parent controller
    //members 
    VendorModalController.Id = "VendorModalController";
    return VendorModalController;
})(); //class
//add the controller to the application
app.controller("VendorModalController", VendorModalController);
//# sourceMappingURL=vendor.modal.controller.js.map