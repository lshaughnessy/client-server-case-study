﻿/*
    File Name: vendor.modal.controller.ts - controller for vendorModal.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class VendorModalController {
    //static injection
    static $inject = ["$modalInstance", "modalData"]; //modalData from parent controller

    //members 
    static Id = "VendorModalController";
    modalTitle: string;
    retVal: any;
    todo: string;

    /*
        constructor
        @param modalInstance: instance of modal not the same as $modal
        @param vendor: vendor instance from in injector loaded from options
    */
    constructor(public modal: ng.ui.bootstrap.IModalServiceInstance, public vendor: Vendor) {
        this.vendor = vendor;
        if (vendor) {
            this.modalTitle = "Update Details for Vendor" + vendor.vendorno;
            this.todo = "update";
        }//end if
        else {
            this.modalTitle = "Add Details for New Vendor";
            this.todo = "add";
        }//end else
        this.retVal = { operation: "", retVendor: vendor, status: "" };
    }//constructor
    
    /*
        NAME:    add()
        PURPOSE: send new vendor back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    add() {
        this.retVal.operation = "add";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }//add

     /*
        NAME:    cancel()
        PURPOSE: discard any changes then back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    cancel() {
        this.retVal.operation = "cancel";
        if (this.vendor) { //we were updating
            this.retVal.status = "Vendor not changed! ";
        }//end if
        else { //we were adding
            this.retVal.status = "No Vendor Entered";
        }//end else
        this.modal.close(this.retVal);
    }//cancel
    
     /*
        NAME:    update()
        PURPOSE: send modified vendor back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    update() {
        this.retVal.operation = "update";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }//update

     /*
        NAME:    delete()
        PURPOSE: send vendor back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    delete() {
        this.retVal.operation = "delete";
        this.retVal.vendor = this.vendor;
        this.modal.close(this.retVal);
    }//delete
}//class

//add the controller to the application
app.controller("VendorModalController", VendorModalController);