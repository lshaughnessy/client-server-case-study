﻿/*
    File Name: vendor.controller.ts -controller for employee.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class VendorController {
    //static injection
    static $inject = ["RESTService", "$modal", "$filter"];

    //members
    vendors: Vendor[];
    status: string;
    selectedRow: number;
    vendor: Vendor;

    /*
        constructor
        @param restsvc: application service for processing all REST calls
    */
    constructor(public restsvc: RESTService, public modal: ng.ui.bootstrap.IModalService, public filter: ng.IFilterService) {
        this.loadVendors();
    }//constructor

    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    public loadVendors(msg?: string) {
        return this.restsvc.callServer("get", "vendor")
            .then((response: Vendor[]) => {
                this.vendors = response;
                if (msg) {
                    this.status = msg + "Vendors Retrieved";
                }//end if
                else {
                    this.status = "Vendors Retrieved";
                }//end else
            })
            .catch((error: any) => this.status = "Vendors not retrieved code - " + error);
    }//loadVendors

    /*
        NAME:    selectRow() 
        PURPOSE: function to determine vendor selected by user then pass desired product on to the new modal
        ACCEPTS: @pram row: row to apply selected row style
                 @param product: selected product to pass to modal
        RETURNS: N/A
    */
    public selectRow(row: number, vendor: Vendor) {
        this.selectedRow = row;
        //set up modal's characteristics
        var options: ng.ui.bootstrap.IModalSettings = {
            templateUrl: "components/vendor/vendorModal.html",
            controller: VendorModalController.Id + " as ctrlr",
            resolve: { modalData: () => { return vendor; } }//data for injection
        };
        //popup the modal
        this.modal.open(options).result
            .then((results: any) => this.processModal(results))
            .catch((error: any) => this.status = error);
    }//selectRow

    /*
        NAME:    findSelected()
        PURPOSE: function to sort products array
        ACCEPTS: @param col: which column are we sorting on
                 @param order: ascending or descending
        RETURNS: N/A
    */
    findSelected(col: number, order: any) {
        this.vendors = this.filter("orderBy")(this.vendors, col, order);
        if (this.vendor) { //have we even selected an vendor?
            for (var i = 0; i < this.vendors.length; i++) { //find selected row
                if (this.vendors[i].vendorno === this.vendor.vendorno) {
                    this.selectedRow = i;
                }//end if
                if (this.vendors[i].vendortype === this.vendor.vendortype) {
                    this.selectedRow = i;
                }
            }//end for
        }//end if
    }//findSelected

   /*
        NAME:    processModal()
        PURPOSE: process product information adter the modal closes
        ACCEPTS: @param results: results object containing info returned from modal
        RETURNS: N/A
   */
    processModal(results: any) {
        var msg = "";

        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "vendor", results.vendor.vendorno, results.vendor)
                    .then((response: any) => {
                        if (parseInt(response, 10) === 1) {
                            msg = "Vendor " + results.vendor.vendorno + " Updated! ";
                            this.loadVendors(msg);
                        }//end if
                    })//end then
                    .catch((error: any) => this.status = "Vendor Not Updated! - " + error);

            case "cancel":
                this.loadVendors(results.status);
                this.selectedRow = -1;
                break;

            case "delete":
                return this.restsvc.callServer("delete", "vendor", results.vendor.vendorno, results.vendor)
                    .then((response: any) => {

                        msg = "Vendor " + results.vendor.vendorno + " Deleted! ";
                        this.loadVendors(msg);
                    }) //end then
                    .catch((error: any) => this.status = "Vendor Cannot be Deleted!" + error);
                break;
            case "add":
                return this.restsvc.callServer("post", "vendor", results.vendor.vendorno, results.vendor)
                    .then((response: any) => {
                        msg = "Vendor Added! ";
                        this.loadVendors(msg);
                    })//end then
                    .catch((error: any) => this.status = "Vendor Not Added!" + error);
                break;
        }//end switch
    }//processModalChanges
}//class

//add the controller to the application
app.controller("VendorController", VendorController);