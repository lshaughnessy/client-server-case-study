/*
    File Name: vendor.controller.ts -controller for employee.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
var VendorController = (function () {
    /*
        constructor
        @param restsvc: application service for processing all REST calls
    */
    function VendorController(restsvc, modal, filter) {
        this.restsvc = restsvc;
        this.modal = modal;
        this.filter = filter;
        this.loadVendors();
    } //constructor
    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    VendorController.prototype.loadVendors = function (msg) {
        var _this = this;
        return this.restsvc.callServer("get", "vendor")
            .then(function (response) {
            _this.vendors = response;
            if (msg) {
                _this.status = msg + "Vendors Retrieved";
            } //end if
            else {
                _this.status = "Vendors Retrieved";
            } //end else
        })
            .catch(function (error) { return _this.status = "Vendors not retrieved code - " + error; });
    }; //loadVendors
    /*
        NAME:    selectRow()
        PURPOSE: function to determine vendor selected by user then pass desired product on to the new modal
        ACCEPTS: @pram row: row to apply selected row style
                 @param product: selected product to pass to modal
        RETURNS: N/A
    */
    VendorController.prototype.selectRow = function (row, vendor) {
        var _this = this;
        this.selectedRow = row;
        //set up modal's characteristics
        var options = {
            templateUrl: "components/vendor/vendorModal.html",
            controller: VendorModalController.Id + " as ctrlr",
            resolve: { modalData: function () { return vendor; } } //data for injection
        };
        //popup the modal
        this.modal.open(options).result
            .then(function (results) { return _this.processModal(results); })
            .catch(function (error) { return _this.status = error; });
    }; //selectRow
    /*
        NAME:    findSelected()
        PURPOSE: function to sort products array
        ACCEPTS: @param col: which column are we sorting on
                 @param order: ascending or descending
        RETURNS: N/A
    */
    VendorController.prototype.findSelected = function (col, order) {
        this.vendors = this.filter("orderBy")(this.vendors, col, order);
        if (this.vendor) {
            for (var i = 0; i < this.vendors.length; i++) {
                if (this.vendors[i].vendorno === this.vendor.vendorno) {
                    this.selectedRow = i;
                } //end if
                if (this.vendors[i].vendortype === this.vendor.vendortype) {
                    this.selectedRow = i;
                }
            } //end for
        } //end if
    }; //findSelected
    /*
         NAME:    processModal()
         PURPOSE: process product information adter the modal closes
         ACCEPTS: @param results: results object containing info returned from modal
         RETURNS: N/A
    */
    VendorController.prototype.processModal = function (results) {
        var _this = this;
        var msg = "";
        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "vendor", results.vendor.vendorno, results.vendor)
                    .then(function (response) {
                    if (parseInt(response, 10) === 1) {
                        msg = "Vendor " + results.vendor.vendorno + " Updated! ";
                        _this.loadVendors(msg);
                    } //end if
                }) //end then
                    .catch(function (error) { return _this.status = "Vendor Not Updated! - " + error; });
            case "cancel":
                this.loadVendors(results.status);
                this.selectedRow = -1;
                break;
            case "delete":
                return this.restsvc.callServer("delete", "vendor", results.vendor.vendorno, results.vendor)
                    .then(function (response) {
                    msg = "Vendor " + results.vendor.vendorno + " Deleted! ";
                    _this.loadVendors(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Vendor Cannot be Deleted!" + error; });
                break;
            case "add":
                return this.restsvc.callServer("post", "vendor", results.vendor.vendorno, results.vendor)
                    .then(function (response) {
                    msg = "Vendor Added! ";
                    _this.loadVendors(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Vendor Not Added!" + error; });
                break;
        } //end switch
    }; //processModalChanges
    //static injection
    VendorController.$inject = ["RESTService", "$modal", "$filter"];
    return VendorController;
})(); //class
//add the controller to the application
app.controller("VendorController", VendorController);
//# sourceMappingURL=vendor.controller.js.map