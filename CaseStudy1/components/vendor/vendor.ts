﻿/*
    File Name: vendor.ts - interface class for vendor information
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
interface Vendor {
    vendorno:   number;
    name:       string;
    address1:   string;
    city:       string;
    province:   string;
    postalcode: string;
    phone:      string;
    vendortype: string;
    email:      string;
}