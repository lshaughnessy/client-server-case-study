﻿/*
    File Name: product.controller.ts - controller for product.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class ProductController {
    //static injection
    static $inject = ["RESTService", "$modal", "$filter"];

    //members
    products: Product[];
    vendors: Vendor[];
    status: string;
    selectedRow: number;
    product: Product;

    /*
        constructor
        @param restsvc: application service for processing all REST calls
    */
    constructor(public restsvc: RESTService, public modal: ng.ui.bootstrap.IModalService, public filter: ng.IFilterService) {
        this.loadVendors();
        this.loadProducts();
    }//constructor

    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    public loadVendors(msg?: string) {
        return this.restsvc.callServer("get", "vendor")
            .then((response: Vendor[]) => {
                this.vendors = response;
                if (msg) {
                    this.status = msg + "Vendors Retrieved";
                }//end if
                else {
                    this.status = "Vendors Retrieved";
                }//end else
            })
            .catch((error: any) => this.status = "Vendors not retrieved code - " + error);
    }//loadVendors

    /*
        NAME:    loadProducts() 
        PURPOSE: called from constructor to call restsvc to return promise containing all product information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    public loadProducts(msg?: string) {
        return this.restsvc.callServer("get", "product")
            .then((response: Product[]) => {
                this.products = response;
                if (msg) {
                    this.status = msg + "Products Retrieved";
                }//end if
                else {
                    this.status = "Products Retrieved";
                }//end else
            })//end then
            .catch((error: any) => this.status = "Products not retrieved code - " + error);
    }//loadProducts

    /*
        NAME:    selectRow() 
        PURPOSE: function to determine product selected by user then pass desired product on to the new modal
        ACCEPTS: @pram row: row to apply selected row style
                 @param product: selected product to pass to modal
        RETURNS: N/A
    */
    public selectRow(row: number, product: Product) {
        var md = { prod: product, vendors: this.vendors };
        //set up modal's characteristics
        var options: ng.ui.bootstrap.IModalSettings = {
            templateUrl: "components/product/productModal.html",
            controller: ProductModalController.Id + " as ctrlr",
            resolve: { modalData: () => { return md; } }//data for injection
        };
        //popup the modal
        this.modal.open(options).result
            .then((results: any) => this.processModal(results))
            .catch((error: any) => this.status = error);
    }//selectRow

    /*
        NAME:    findSelected()
        PURPOSE: function to sort products array
        ACCEPTS: @param col: which column are we sorting on
                 @param order: ascending or descending
        RETURNS: N/A
    */
    findSelected(col: number, order: any) {
        this.products = this.filter("orderBy")(this.products, col, order);
        if (this.product) { //have we even selected an product?
            for (var i = 0; i < this.products.length; i++) { //find selected row
                if (this.products[i].productcode === this.product.productcode) {
                    this.selectedRow = i;
                }//end if
            }//end for
        }//end if
    }//findSelected

    /*
        NAME:    processModal()
        PURPOSE: process product information adter the modal closes
        ACCEPTS: @param results: results object containing info returned from modal
        RETURNS: N/A
    */
    processModal(results: any) {
        var msg = "";

        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "product", results.product.productcode, results.product)
                    .then((response: any) => {
                        if (parseInt(response, 10) === 1) {
                            msg = "Product " + results.product.productcode + " Updated! ";
                            this.loadProducts(msg);
                        }//end if
                    })//end then
                    .catch((error: any) => this.status = "Product Not Updated! - " + error);

            case "cancel":
                this.loadProducts(results.status);
                this.selectedRow = -1;
                break;

            case "delete":
                return this.restsvc.callServer("delete", "product", results.product.productcode, results.product)
                    .then((response: any) => {

                        msg = "Product " + results.product.productcode + " Deleted! ";
                        this.loadProducts(msg);
                    }) //end then
                    .catch((error: any) => this.status = "Product Cannot be Deleted!" + error);
                break;

            case "add":
                return this.restsvc.callServer("post", "product", results.product.productcode, results.product)
                    .then((response: any) => {
                        msg = "Product Added! ";
                        this.loadProducts(msg);
                    })//end then
                    .catch((error: any) => this.status = "Product Not Added!" + error);
                break;
        }//end switch
    }//processModalChanges
}//class

//add the controller to the application
app.controller("ProductController", ProductController);