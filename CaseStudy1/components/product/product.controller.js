/*
    File Name: product.controller.ts - controller for product.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
var ProductController = (function () {
    /*
        constructor
        @param restsvc: application service for processing all REST calls
    */
    function ProductController(restsvc, modal, filter) {
        this.restsvc = restsvc;
        this.modal = modal;
        this.filter = filter;
        this.loadVendors();
        this.loadProducts();
    } //constructor
    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    ProductController.prototype.loadVendors = function (msg) {
        var _this = this;
        return this.restsvc.callServer("get", "vendor")
            .then(function (response) {
            _this.vendors = response;
            if (msg) {
                _this.status = msg + "Vendors Retrieved";
            } //end if
            else {
                _this.status = "Vendors Retrieved";
            } //end else
        })
            .catch(function (error) { return _this.status = "Vendors not retrieved code - " + error; });
    }; //loadVendors
    /*
        NAME:    loadProducts()
        PURPOSE: called from constructor to call restsvc to return promise containing all product information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    ProductController.prototype.loadProducts = function (msg) {
        var _this = this;
        return this.restsvc.callServer("get", "product")
            .then(function (response) {
            _this.products = response;
            if (msg) {
                _this.status = msg + "Products Retrieved";
            } //end if
            else {
                _this.status = "Products Retrieved";
            } //end else
        }) //end then
            .catch(function (error) { return _this.status = "Products not retrieved code - " + error; });
    }; //loadProducts
    /*
        NAME:    selectRow()
        PURPOSE: function to determine product selected by user then pass desired product on to the new modal
        ACCEPTS: @pram row: row to apply selected row style
                 @param product: selected product to pass to modal
        RETURNS: N/A
    */
    ProductController.prototype.selectRow = function (row, product) {
        var _this = this;
        var md = { prod: product, vendors: this.vendors };
        //set up modal's characteristics
        var options = {
            templateUrl: "components/product/productModal.html",
            controller: ProductModalController.Id + " as ctrlr",
            resolve: { modalData: function () { return md; } } //data for injection
        };
        //popup the modal
        this.modal.open(options).result
            .then(function (results) { return _this.processModal(results); })
            .catch(function (error) { return _this.status = error; });
    }; //selectRow
    /*
        NAME:    findSelected()
        PURPOSE: function to sort products array
        ACCEPTS: @param col: which column are we sorting on
                 @param order: ascending or descending
        RETURNS: N/A
    */
    ProductController.prototype.findSelected = function (col, order) {
        this.products = this.filter("orderBy")(this.products, col, order);
        if (this.product) {
            for (var i = 0; i < this.products.length; i++) {
                if (this.products[i].productcode === this.product.productcode) {
                    this.selectedRow = i;
                } //end if
            } //end for
        } //end if
    }; //findSelected
    /*
        NAME:    processModal()
        PURPOSE: process product information adter the modal closes
        ACCEPTS: @param results: results object containing info returned from modal
        RETURNS: N/A
    */
    ProductController.prototype.processModal = function (results) {
        var _this = this;
        var msg = "";
        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    if (parseInt(response, 10) === 1) {
                        msg = "Product " + results.product.productcode + " Updated! ";
                        _this.loadProducts(msg);
                    } //end if
                }) //end then
                    .catch(function (error) { return _this.status = "Product Not Updated! - " + error; });
            case "cancel":
                this.loadProducts(results.status);
                this.selectedRow = -1;
                break;
            case "delete":
                return this.restsvc.callServer("delete", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    msg = "Product " + results.product.productcode + " Deleted! ";
                    _this.loadProducts(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Product Cannot be Deleted!" + error; });
                break;
            case "add":
                return this.restsvc.callServer("post", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    msg = "Product Added! ";
                    _this.loadProducts(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Product Not Added!" + error; });
                break;
        } //end switch
    }; //processModalChanges
    //static injection
    ProductController.$inject = ["RESTService", "$modal", "$filter"];
    return ProductController;
})(); //class
//add the controller to the application
app.controller("ProductController", ProductController);
//# sourceMappingURL=product.controller.js.map