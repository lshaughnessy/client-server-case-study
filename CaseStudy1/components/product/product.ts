﻿/*
    File Name: product.ts - interface for product information
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
interface Product {
    //members
    productcode: string;
    vendorno: number;
    vendorsku: string;
    productname: string;
    costprice: number;
    msrp: number;
    rop: number;
    eoq: number;
    qoh: number;
    qoo: number;
    qrcode: Blob;
    qrtext: string;
}