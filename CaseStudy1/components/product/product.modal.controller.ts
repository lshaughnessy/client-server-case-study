﻿/*
    File Name: product.modal.controller.ts - controller for productModal.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class ProductModalController {
    //static injection
    static $inject = ["$modalInstance", "modalData"]; //modalData from parent controller

    //members 
    static Id = "ProductModalController";
    modalTitle: string;
    retVal: any;
    todo: string;
    product: Product;
    vendors: Vendor[];

    /*
        constructor
        @param modalInstance: instance of modal not the same as $modal
        @param product: product instance from in injector loaded from options
    */
    constructor(public modal: ng.ui.bootstrap.IModalServiceInstance, modalData: ProductModalData) {
        this.product = modalData.prod;
        this.vendors = modalData.vendors;
        if (this.product) {
            this.modalTitle = "Update Details for Product " + this.product.productname;
            this.todo = "update";
        }//end if
        else {
            this.modalTitle = "Add Details for New Product";
            this.todo = "add";
        }//end else
        this.retVal = { operation: "", retProduct: this.product, status: "" };
    }//constructor
    
    /*
        NAME:    add()
        PURPOSE: send new product back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    add() {
        this.retVal.operation = "add";
        this.retVal.product = this.product;
        this.modal.close(this.retVal);
    }//add

    /*
        NAME:    cancel()
        PURPOSE: discard any changes then back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    cancel() {
        this.retVal.operation = "cancel";
        if (this.product) { //we were updating
            this.retVal.status = "Product not changed! ";
        }//end if
        else { //we were adding
            this.retVal.status = "No Product Entered";
        }//end else
        this.modal.close(this.retVal);
    }//cancel

    /*
        NAME:    update()
        PURPOSE: send modified product back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    update() {
        this.retVal.operation = "update";
        this.retVal.product = this.product;
        this.modal.close(this.retVal);
    }//update

    /*
        NAME:    delete()
        PURPOSE: send product back to the main controller
        ACCEPTS: N/A
        RETURNS: N/A
    */
    delete() {
        this.retVal.operation = "delete";
        this.retVal.product = this.product;
        this.modal.close(this.retVal);
    }//delete

}//class
//add the controller to the application
app.controller("ProductModalController", ProductModalController);