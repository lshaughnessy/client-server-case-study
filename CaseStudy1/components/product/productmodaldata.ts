﻿/*
    File Name: productmodaldata.ts - interface for passing product data to modal
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
interface ProductModalData {
    prod: Product;
    vendors: Vendor[];
}