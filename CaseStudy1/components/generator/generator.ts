﻿/*
    File Name: generator.ts - container class for line item in generator
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/

interface GeneratorItem {
    //class members
    productcode: string;
    productname: string;
    price: number;
    ext: number;
    qty: number;
}