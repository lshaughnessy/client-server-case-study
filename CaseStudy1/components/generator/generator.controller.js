/*
    File Name: GeneratorController - controller for generator.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
var GeneratorController = (function () {
    /*
        constructor
        @param restsvc: application service for processing all REST calls
        @param windowsvc: angular window service for
    */
    function GeneratorController(restsvc, windowsvc) {
        this.restsvc = restsvc;
        this.windowsvc = windowsvc;
        this.initThings();
        this.loadVendors();
    } //constructor
    /*
        NAME: initThings()
        PURPOSE: resets the items array and the calculations for a dynamic total on products
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.initThings = function () {
        this.items = [];
        this.tax, this.subTotal, this.total = 0;
    };
    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    GeneratorController.prototype.loadVendors = function (msg) {
        var _this = this;
        return this.restsvc.callServer("get", "vendor")
            .then(function (response) {
            _this.vendors = response;
            if (msg) {
                _this.status = msg + "Vendors Retrieved";
            } //end if
            else {
                _this.status = "Vendors Retrieved";
            } //end else
        })
            .catch(function (error) { return _this.status = "Vendors not retrieved code - " + error; });
    }; //loadVendors
    /*
        NAME:    LoadProductsFromVendor()
        PURPOSE: will load the products for a selected Vendor. It will also reset/clear when the user switches to a different vendor
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    GeneratorController.prototype.LoadProductsFromVendor = function (msg) {
        var _this = this;
        return this.restsvc.callServer("get", "product/" + this.vendor.vendorno)
            .then(function (response) {
            _this.products = response;
            _this.initThings();
            _this.status = "Products Retrieved";
        }) //end then
            .catch(function (error) { return _this.status = "Products not retrieved code - " + error; });
    }; //LoadProductsFromVendor
    /*
        NAME:    processModal()
        PURPOSE: process product information adter the modal closes
        ACCEPTS: @param results: results object containing info returned from modal
        RETURNS: N/A
    */
    GeneratorController.prototype.processModal = function (results) {
        var _this = this;
        var msg = "";
        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    if (parseInt(response, 10) === 1) {
                        msg = "Product " + results.product.productcode + " Updated! ";
                        _this.LoadProductsFromVendor(msg);
                    } //end if
                }) //end then
                    .catch(function (error) { return _this.status = "Product Not Updated! - " + error; });
            case "cancel":
                this.LoadProductsFromVendor(results.status);
                break;
            case "delete":
                return this.restsvc.callServer("delete", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    msg = "Product " + results.product.productcode + " Deleted! ";
                    _this.LoadProductsFromVendor(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Product Cannot be Deleted!" + error; });
                break;
            case "add":
                return this.restsvc.callServer("post", "product", results.product.productcode, results.product)
                    .then(function (response) {
                    msg = "Product Added! ";
                    _this.LoadProductsFromVendor(msg);
                }) //end then
                    .catch(function (error) { return _this.status = "Product Not Added!" + error; });
                break;
        } //end switch
    }; //processModal
    /*
        NAME:    calculateTotal()
        PURPOSE: calculates the running total of products a customer wants to buy (subtotal, tax, and the total)
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.calculateTotal = function () {
        var toCalc = 0;
        this.items.forEach(function (value, index) {
            toCalc += value.ext;
        }); //end forEach
        this.subTotal = toCalc;
        this.tax = this.subTotal * 0.13;
        this.total = this.subTotal + this.tax;
    }; //calculateTotal()
    /*
        NAME:    addItem()
        PURPOSE: Keeps track of non-zero products being added to the purchase order.
                 If the quantity is EOQ, retrieve the number set and calculate the total for that quantity
                 If the quantity is 0, clear the item from the list, otherwise, add it to the list and calculate the total.
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.addItem = function () {
        var _this = this;
        if (this.item.qty != 0) {
            this.products.forEach(function (value, index) {
                if (value.productname == _this.item.productname) {
                    var indexOfItem = _this.items.map(function (pos) { return pos.productname; }).indexOf(_this.item.productname);
                    if (indexOfItem == -1) {
                        var quantity = _this.item.qty.toString() == "EOQ" ? value.eoq : _this.item.qty;
                        _this.items.push({
                            productname: value.productname,
                            productcode: value.productcode,
                            qty: quantity,
                            price: value.costprice,
                            ext: quantity * value.costprice
                        }); //end this
                        _this.status = "Item Added!";
                    } //end if
                    else {
                        _this.items[indexOfItem].productcode = value.productcode;
                        _this.items[indexOfItem].price = value.costprice;
                        if (_this.item.qty.toString() == "EOQ") {
                            _this.items[indexOfItem].qty = value.eoq;
                        } //end else
                        else {
                            _this.items[indexOfItem].qty = _this.item.qty;
                        } //end else
                        _this.items[indexOfItem].ext = _this.items[indexOfItem].price * _this.items[indexOfItem].qty;
                        _this.status = "Item has been Updated";
                    } //end else
                } //end if
            }); //end forEach
        } //end if
        else {
            var indexOfItem = this.items.map(function (pos) { return pos.productname; }).indexOf(this.item.productname);
            if (indexOfItem != -1) {
                this.items.splice(indexOfItem, 1);
                this.status = "Item Removed";
            } //end if
        } // end else 
        this.generated = false;
        this.calculateTotal();
        if (this.items.length == 0) {
            this.status = "No Items";
        } //end if
    }; //addItem()
    /*
        NAME:    orderDate()
        PURPOSE: properly format the PoDate
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.orderDate = function () {
        var split = new Date().toLocaleDateString().split('/').reverse();
        var temp = split[1];
        split[1] = split[2];
        split[2] = temp;
        return split.join('-');
    }; //orderDate()
    /*
        NAME:    viewPDF()
        PURPOSE: determine po number and pass to server side servlet for PDF generation
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.viewPDF = function () {
        this.windowsvc.location.href = "http://localhost:8080/APPOCase1/POPDF?po=" + this.pono;
    }; //viewPDF()
    /*
        NAME:    changeVendor()
        PURPOSE: resets the fields when the user changes to a different vendor
        ACCEPTS: N/A
        RETURNS: N/A
    */
    GeneratorController.prototype.changeVendor = function () {
        this.products = undefined;
        this.items = undefined;
        this.status = undefined;
        this.vendor = undefined;
        this.item = undefined;
        this.tax = 0;
        this.subTotal = 0;
        this.total = 0;
        return null;
    }; //changeVendor()
    /*
        NAME:    createPO()
        PURPOSE: functionality for button to create a purchase order for customer
        ACCEPTS: N/A
        RETURNS: Post (add) purchase order
    */
    GeneratorController.prototype.createPO = function () {
        var _this = this;
        this.status = "Wait...";
        var PODTO = {
            vendorno: this.vendor.vendorno,
            ponumber: -1,
            items: this.items,
            total: this.total,
            podate: this.orderDate()
        }; //end var
        return this.restsvc.callServer("post", "po", "", PODTO)
            .then(function (generatedPonumber) {
            if (generatedPonumber > 0) {
                _this.status = "PO " + generatedPonumber + " Created!";
                _this.pono = generatedPonumber;
                _this.generated = true;
            } //end if
            else {
                _this.status = "Problem generating PO, contact purchasing";
            } //end else
        }) //end then
            .then(this.changeVendor())
            .catch(function (error) { return _this.status = "PO not created - " + error; });
    }; //createPTO()
    //static injection
    GeneratorController.$inject = ["RESTService", "$window"];
    return GeneratorController;
})(); //class
//add the controller to the application
app.controller("GeneratorController", GeneratorController);
//# sourceMappingURL=generator.controller.js.map