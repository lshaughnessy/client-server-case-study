﻿/*
    File Name: GeneratorController - controller for generator.html partial
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
class GeneratorController {
    //static injection
    static $inject = ["RESTService", "$window"];

    //members
    products: Product[];
    items: GeneratorItem[];
    vendors: Vendor[];
    status: string;
    vendor: Vendor;
    item: GeneratorItem;
    tax: number;
    subTotal: number;
    total: number;

    pono: number;
    generated: boolean;

    /*
        constructor
        @param restsvc: application service for processing all REST calls
        @param windowsvc: angular window service for 
    */
    constructor(public restsvc: RESTService, public windowsvc: ng.IWindowService) {
        this.initThings();
        this.loadVendors();
    }//constructor

    /*
        NAME: initThings()
        PURPOSE: resets the items array and the calculations for a dynamic total on products
        ACCEPTS: N/A
        RETURNS: N/A
    */
    public initThings() {
        this.items = [];
        this.tax, this.subTotal, this.total = 0;
    }

    /*
        NAME:    loadVendors()
        PURPOSE: called from constructor to call restsvc to return promise containing all vendor information from server
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    public loadVendors(msg?: string) {
        return this.restsvc.callServer("get", "vendor")
            .then((response: Vendor[]) => {
                this.vendors = response;
                if (msg) {
                    this.status = msg + "Vendors Retrieved";
                }//end if
                else {
                    this.status = "Vendors Retrieved";
                }//end else
            })
            .catch((error: any) => this.status = "Vendors not retrieved code - " + error);
    }//loadVendors

    /*
        NAME:    LoadProductsFromVendor()
        PURPOSE: will load the products for a selected Vendor. It will also reset/clear when the user switches to a different vendor
        ACCEPTS: @param msg: optional message coming from prior process
        RETURNS: N/A
    */
    public LoadProductsFromVendor(msg?: string) {
        return this.restsvc.callServer("get", "product/" + this.vendor.vendorno)
            .then((response: Product[]) => {
                this.products = response;
                this.initThings();
                this.status = "Products Retrieved";
            })//end then
            .catch((error: any) => this.status = "Products not retrieved code - " + error);
    }//LoadProductsFromVendor

    /*
        NAME:    processModal()
        PURPOSE: process product information adter the modal closes
        ACCEPTS: @param results: results object containing info returned from modal
        RETURNS: N/A
    */
    processModal(results: any) {
        var msg = "";

        switch (results.operation) {
            case "update":
                return this.restsvc.callServer("put", "product", results.product.productcode, results.product)
                    .then((response: any) => {
                        if (parseInt(response, 10) === 1) {
                            msg = "Product " + results.product.productcode + " Updated! ";
                            this.LoadProductsFromVendor(msg);
                        }//end if
                    })//end then
                    .catch((error: any) => this.status = "Product Not Updated! - " + error);

            case "cancel":
                this.LoadProductsFromVendor(results.status);
                break;

            case "delete":
                return this.restsvc.callServer("delete", "product", results.product.productcode, results.product)
                    .then((response: any) => {
                        msg = "Product " + results.product.productcode + " Deleted! ";
                        this.LoadProductsFromVendor(msg);
                    }) //end then
                    .catch((error: any) => this.status = "Product Cannot be Deleted!" + error);
                break;

            case "add":
                return this.restsvc.callServer("post", "product", results.product.productcode, results.product)
                    .then((response: any) => {
                        msg = "Product Added! ";
                        this.LoadProductsFromVendor(msg);
                    })//end then
                    .catch((error: any) => this.status = "Product Not Added!" + error);
                break;
        }//end switch
    }//processModal

    /*
        NAME:    calculateTotal()
        PURPOSE: calculates the running total of products a customer wants to buy (subtotal, tax, and the total)
        ACCEPTS: N/A
        RETURNS: N/A
    */
    public calculateTotal() {
        var toCalc = 0;
        this.items.forEach((value: GeneratorItem, index: number) => {
            toCalc += value.ext;
        });//end forEach
        this.subTotal = toCalc;
        this.tax = this.subTotal * 0.13;
        this.total = this.subTotal + this.tax;
    }//calculateTotal()

    /*
        NAME:    addItem()
        PURPOSE: Keeps track of non-zero products being added to the purchase order. 
                 If the quantity is EOQ, retrieve the number set and calculate the total for that quantity
                 If the quantity is 0, clear the item from the list, otherwise, add it to the list and calculate the total.
        ACCEPTS: N/A
        RETURNS: N/A
    */
    private addItem() {
        if (this.item.qty != 0) {
            this.products.forEach((value: Product, index: number) => {

                if (value.productname == this.item.productname) {
                    var indexOfItem = this.items.map(function (pos) { return pos.productname; }).indexOf(this.item.productname);

                    if (indexOfItem == -1) {
                        var quantity = this.item.qty.toString() == "EOQ" ? value.eoq : this.item.qty;
                        this.items.push({
                            productname: value.productname,
                            productcode: value.productcode,
                            qty: quantity,
                            price: value.costprice,
                            ext: quantity * value.costprice
                        });//end this
                        this.status = "Item Added!";
                    }//end if
                    else {
                        this.items[indexOfItem].productcode = value.productcode;
                        this.items[indexOfItem].price = value.costprice;
                        if (this.item.qty.toString() == "EOQ") {
                            this.items[indexOfItem].qty = value.eoq;
                        }//end else
                        else {
                            this.items[indexOfItem].qty = this.item.qty;
                        }//end else
                        this.items[indexOfItem].ext = this.items[indexOfItem].price * this.items[indexOfItem].qty;
                        this.status = "Item has been Updated";
                    }//end else
                }//end if
            });//end forEach
        }//end if
        else {
            var indexOfItem = this.items.map(function (pos) { return pos.productname; }).indexOf(this.item.productname);
            if (indexOfItem != -1) {
                this.items.splice(indexOfItem, 1);
                this.status = "Item Removed";
            }//end if
        }// end else 
        this.generated = false;
        this.calculateTotal();
        if (this.items.length == 0)
        {
            this.status = "No Items";
        }//end if
    }//addItem()

    /*
        NAME:    orderDate()
        PURPOSE: properly format the PoDate
        ACCEPTS: N/A
        RETURNS: N/A
    */
    private orderDate() {
        var split = new Date().toLocaleDateString().split('/').reverse();

        var temp = split[1];
        split[1] = split[2];
        split[2] = temp;

        return split.join('-');
    }//orderDate()

    /*
        NAME:    viewPDF()
        PURPOSE: determine po number and pass to server side servlet for PDF generation
        ACCEPTS: N/A
        RETURNS: N/A
    */
    private viewPDF() {
        this.windowsvc.location.href = "http://localhost:8080/APPOCase1/POPDF?po=" + this.pono;
    }//viewPDF()

    /*
        NAME:    changeVendor()
        PURPOSE: resets the fields when the user changes to a different vendor
        ACCEPTS: N/A
        RETURNS: N/A
    */
    private changeVendor() {
        this.products   = undefined;
        this.items      = undefined;
        this.status     = undefined;
        this.vendor     = undefined;
        this.item       = undefined;
        this.tax        = 0;
        this.subTotal   = 0;
        this.total      = 0;
        return null;
    }//changeVendor()

    /*
        NAME:    createPO()
        PURPOSE: functionality for button to create a purchase order for customer
        ACCEPTS: N/A
        RETURNS: Post (add) purchase order
    */
    private createPO() {
        this.status = "Wait...";
        var PODTO: PoDTO = {
            vendorno: this.vendor.vendorno,
            ponumber: -1,
            items: this.items,
            total: this.total,
            podate: this.orderDate()
        };//end var
        return this.restsvc.callServer("post", "po", "", PODTO)
            .then((generatedPonumber: number) => {
                if (generatedPonumber > 0) {
                    this.status = "PO " + generatedPonumber + " Created!";
                    this.pono = generatedPonumber;
                    this.generated = true;
                }//end if
                else {
                    this.status = "Problem generating PO, contact purchasing";
                }//end else
            })//end then
            .then(this.changeVendor())
            .catch((error: any) => this.status = "PO not created - " + error);
    }//createPTO()
}//class

//add the controller to the application
app.controller("GeneratorController", GeneratorController);