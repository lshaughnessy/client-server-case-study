﻿/*
    File Name: poDTO.ts - container class for purchase order in generator
    Coder: Lexi Shaughnessy
    Student Number: 0658874
    Date: October 23, 2015
*/
interface PoDTO {
    vendorno: number;
    ponumber: number;
    items: GeneratorItem[];
    total: number;
    podate: string;
}